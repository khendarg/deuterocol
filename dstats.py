from __future__ import division, print_function
import scipy
import scipy.stats
import os
import random
class Classifier(object):
	def __init__(self, test_size=0.1, max_iterations=20, seed=None):
		if seed is not None: random.seed(seed)
		self.seed = seed
		self.max_iterations = max_iterations
		self.test_size = test_size

		self.kdes = {}


	def train(self, **rawclasses):
		classes = {}
		for k in rawclasses:
			classes[k] = rawclasses[k][:]
			random.shuffle(classes[k])
		#TODO: find out if there's a better way to balance positive:negative

		sets = []
		kdes = []
		for i in range(int(scipy.ceil(1/self.test_size))):
			sets.append({})
			kdes.append({})
			for k in classes:
				sets[-1][k] = []
				start = int(scipy.floor(i*self.test_size*len(classes[k])))
				stop = int(scipy.ceil((i+1)*self.test_size*len(classes[k])))

				sets[-1][k] = classes[k][start:stop]
				#print(k, start, stop, classes[k][start:stop])

				sets[-1][k] = scipy.array(sets[-1][k]).T
				#kdes[-1][k] = scipy.stats.gaussian_kde(sets[-1][k])
				
		indices = {}
		for k in classes:
			indices[k] = []
			for i in range(int(scipy.ceil(1/self.test_size))):
				start = int(scipy.floor(i*self.test_size*len(classes[k])))
				indices[k].append(start)
			indices[k].append(None)
		

		for i in range(int(scipy.ceil(1/self.test_size))):
			maxi = int(scipy.ceil(1/self.test_size))
			mini = 0

			trains = {}
			tests = []
			for k in classes:
				trains[k] = []

				a = 0
				b = indices[k][i]
				c = indices[k][min(maxi, i+1)]
				d = None

				for vec in classes[k][b:c]: tests.append((vec, k))
				for vec in classes[k][a:b]: trains[k].append(vec)
				for vec in classes[k][c:d]: trains[k].append(vec)

			kdes = {}
			for k in classes:
				kdes[k] = scipy.stats.gaussian_kde(scipy.array(trains[k]).T)

			PRIOR = 0.5
			tp, tn, fp, fn, err = 0, 0, 0, 0, 0
			for vec, trueclass in tests:
				kpairs = set()
				for k1 in (classes):
					for k2 in (classes):
						if k1 == k2: continue
						elif k1+k2 in kpairs: continue
						elif k2+k1 in kpairs: continue
						else: kpairs.add(k1+k2)
						posterior = kdes[k1].evaluate(vec) * PRIOR / (kdes[k1].evaluate(vec) * PRIOR + kdes[k2].evaluate(vec) * (1 - PRIOR))
						if posterior >= 0.5:
							if trueclass == k1: tp += 1
							elif trueclass == k2: fp += 1
							else: err += 1
						else:
							if trueclass == k1: fn += 1
							elif trueclass == k2: tn += 1
							else: err += 1

			#print('TP = {}\tFP = {}'.format(tp, fp))
			#print('FN = {}\tTN = {}'.format(fn, tn))
			#print('err = {}'.format(err))
			prec = tp/(tp+fp)
			rec = tp/(tp+fn)
			
			self.kdes = kdes
			print('precision = {:0.4f}, recall = {:0.4f} (n = {})'.format(prec, rec, tp+fp+tn+fn))
			return kdes
			

	def classify(self, test_set):
		classes = [k for k in self.kdes]
		PRIOR = 0.5
		posteriors = {}
		for vec in test_set:
			kpairs = set()
			for k1 in classes:
				if k1 not in posteriors: posteriors[k1] = {}
				for k2 in classes:
					if k2 not in posteriors[k1]: posteriors[k1][k2] = []
					if k1 == k2: continue
					elif k1+k2 in kpairs: continue
					elif k2+k1 in kpairs: continue
					else: kpairs.add(k1+k2)

					posterior = self.kdes[k1].evaluate(vec) * PRIOR / (self.kdes[k1].evaluate(vec) * PRIOR + self.kdes[k2].evaluate(vec) * (1 - PRIOR))
					posteriors[k1][k2].append(posterior)

		for k1 in posteriors:
			for k2 in posteriors[k1]:
				#for post in posteriors[k1][k2]: print('\t{:0.5f}'.format(post))
				for post in posteriors[k1][k2]: print('\t{}'.format(post))
				print(scipy.mean(posteriors[k1][k2]), scipy.std(posteriors[k1][k2]))

		return posteriors
					

class AlignmentSet(object):
	def __init__(self, alignments=None):
		self.alignments = set() 
		self._hashes = set()
		for aln in self.alignments: 
			if hash(aln) in self._hashes: continue
			self.alignments.add(aln)
			self._hashes.add(hash(aln))

	def extend(self, other):
		for aln in other: 
			if hash(aln) in self._hashes: continue
			self.alignments.add(aln)
			self._hashes.add(hash(aln))

	def add(self, aln):
		if hash(aln) in self._hashes: pass
		else:
			self.alignments.add(aln)
			self._hashes.add(hash(aln))

	def __len__(self): return len(self.alignments)

	def __iter__(self): return iter(self.alignments)

class ChainPair(object):
	def __init__(self, label='', alignments=None):
		self.label = label
		self.alignments = AlignmentSet() if alignments is None else alignments

	def add(self, aln): self.alignments.add(aln)

	def __len__(self): return len(self.alignments)

	def __iter__(self): return iter(self.alignments)

	def sort(self, method):
		#returns sorted alignments worst to best
		if method is None: method = 'covqual'

		if method == 'covqual':
			for aln in self: aln._sortindex = (aln.tmcov, aln.quality)
		elif method == 'covrmsd':
			for aln in self: aln._sortindex = aln.tmcov / aln.rmsd
		elif method == 'quality':
			for aln in self: aln._sortindex = aln.quality
		elif method == 'coverage':
			for aln in self: aln._sortindex = aln.tmcov
		elif method == 'rmsd':
			for aln in self: aln._sortindex = -aln.rmsd
		elif method == 'length':
			for aln in self: aln._sortindex = aln.length
		elif method == 'alphabet':
			for aln in self: aln._sortindex = aln.name
		elif method == 'euclid1':
			for aln in self: aln._sortindex = -(1*(1.0 - aln.tmcov)**2 + (aln.rmsd)**2)
		elif method == 'euclid2':
			for aln in self: aln._sortindex = -(2*(1.0 - aln.tmcov)**2 + (aln.rmsd)**2)
		elif method == 'euclid3':
			for aln in self: aln._sortindex = -(3*(1.0 - aln.tmcov)**2 + (aln.rmsd)**2)
		elif method == 'euclid4':
			for aln in self: aln._sortindex = -(4*(1.0 - aln.tmcov)**2 + (aln.rmsd)**2)
			#covrmsd, quality, coverage, rmsd, length, alphabet, cartesian, euclidean

		return sorted(self.alignments)
		

	def __getitem__(self, index):
		pass

class Alignment(object):
	name = ''
	query = ''
	qchain = ''
	subject = ''
	schain = ''
	rmsd = -1.
	quality = -1.
	length = -1
	tmcov = -1.
	fullcov = -1.

	text = ''

	_sortindex = 0
	def __init__(self, qhel=None, shel=None):
		qhel = [] if qhel is None else qhel
		shel = [] if shel is None else shel

	def __lt__(self, other):
		if self._sortindex < other._sortindex: return True
		else: return False

	def __hash__(self):
		return hash(str(self.name) + repr(self.name))

	def __str__(self):
		if self.text: return self.text
		else: return '{}\t{}\t{}\t{}\t{:0.2%}\t'.format(self.name, self.quality, self.rmsd, self.length, self.tmcov)

class D2Parser(object):
	def parse(self, indir):
		pass

class TSVParser(object):
	def parse(self, infile):
		by_chainpair = {}
		for l in infile:
			if not l.strip(): continue
			elif l.lstrip().startswith('#'): continue

			sl = l.split('\t')
			obj = Alignment()
			obj.name = sl[0]
			obj.query = sl[1]
			obj.qchain = sl[2]
			obj.qhel = sl[3][1:].split('-')
			obj.subject = sl[4]
			obj.schain = sl[5]
			obj.shel = sl[6][1:].split('-')
			obj.rmsd = float(sl[7])
			obj.quality = float(sl[8])
			obj.length = float(sl[9])
			obj.tmcov = float(sl[10][:-1])/100
			obj.fullcov = float(sl[11][:-1])/100
			obj.text = l.replace('\n', '')

			chainpair = '{}_{}_{}_{}'.format(obj.query, obj.qchain, obj.subject, obj.schain)
			#by_chainpair[chainpair].add(obj)
			try: by_chainpair[chainpair].add(obj)
			except KeyError: 
				by_chainpair[chainpair] = ChainPair(label=chainpair)
				by_chainpair[chainpair].add(obj)
		return by_chainpair

def winnow(infiles, method=None, number=1, overlap=False):
	all_chainpairs = {}
	for fn in infiles:
		if os.path.isfile(fn): 
			p = TSVParser()
			with open(fn) as f: by_chainpair = p.parse(f)
			for pair in by_chainpair: all_chainpairs[pair] = by_chainpair[pair]
		elif os.path.isdir(fn):
			p = D2Parser()
			#all_aln.extend(p.parse(fn))
			#TODO: integrate 
		else: raise IOError('No such file or directory: {}'.format(fn))

	selected_alignments = []
	donehels = {}

	for pair in all_chainpairs:
		donehels[pair] = {'q':set(), 's':set()}
		sorted_alns = all_chainpairs[pair].sort(method)[::-1]

		if overlap: selected_alignments.extend(sorted_alns[:number])
		else:
			added = 0
			for aln in sorted_alns:
				if added >= number: break
				skipme = False

				for i in range(int(aln.qhel[0]), int(aln.qhel[1])+1):
					if i in donehels[pair]['q']:
						skipme = True
						break
				if skipme: continue

				for i in range(int(aln.shel[0]), int(aln.shel[1])+1):
					if i in donehels[pair]['s']:
						skipme = True
						break
				if skipme: continue

				for i in range(int(aln.qhel[0]), int(aln.qhel[1])+1):
					donehels[pair]['q'].add(i)
				for i in range(int(aln.shel[0]), int(aln.shel[1])+1):
					donehels[pair]['s'].add(i)

				selected_alignments.append(aln)
				added += 1
				if added >= number: break

	#for aln in sorted(selected_alignments)[::-1]: 
	#	print(aln)
	return sorted(selected_alignments)[::-1]


