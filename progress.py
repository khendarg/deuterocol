from __future__ import print_function, division
import os
import argparse
import time

def print_progressbar(done, queued, width=80):
	s = '['
	colsdone = int(round(done/queued*(width-2))) 
	s += colsdone * '#'
	s += (width - 2 - colsdone) * '-'
	s += ']'
	print(s)

def print_remaining_time(deltabuffer, total):
	deltas = [deltabuffer[i+1][0] - deltabuffer[i][0] for i in range(len(deltabuffer)-1)]
	totals = [deltabuffer[i+1][1] - deltabuffer[i][1] for i in range(len(deltabuffer)-1)]
	rates = sorted(zip(deltas, totals))

	if not rates: return

	median_rate = rates[len(rates)//2]
	mean_rate = sum(deltas), sum(totals)

	central_rate = mean_rate
	if central_rate[1]: timeleft = central_rate[0]/central_rate[1] * (total - deltabuffer[-1][1])
	else: timeleft = 0
	#print(timeleft)

	s = ''
	days, hours, minutes, seconds = 0, 0, 0, 0
	while timeleft > 0:
		if timeleft >= 86400:
			days += 1
			timeleft -= 86400
		elif timeleft >= 3600:
			hours += 1
			timeleft -= 3600
		elif timeleft >= 60:
			minutes += 1
			timeleft -= 60
		else: 
			seconds += timeleft
			timeleft -= timeleft
	if days: s += '{}d'.format(days)
	if hours: s += '{}h'.format(hours)
	if minutes: s += '{}m'.format(minutes)
	if seconds: s += '{}s'.format(int(round(seconds)))
	print('{} left ({} comparisons @ {:0.1f}comps/s)'.format(s, total - central_rate[1], central_rate[1]/central_rate[0]))

def main(paths, interval=10, once=False):


	totals = {}
	totaltodo = 0
	for path in paths: 
		totals[path] = 0
		try:
			with open('{}/config/agenda.json'.format(path)) as f:
				for l in f: 
					totals[path] += 1
					totaltodo += 1
		except IOError: pass

	finished = {}

	deltabuffer = []

	run = True
	while run:
		print(time.strftime('%Y-%m-%d %H:%M:%S'))
		totaldone = 0
		for path in paths:
			try: done = finished[path]
			except KeyError:
				done = 0
				try:
					with open('{}/tmalignments/sp_all.tsv'.format(path)) as f:
						for l in f: done += 1
				except IOError: pass
				if done == totals[path]: finished[path] = done
			totaldone += done
			print('{}: {}/{}'.format(os.path.basename(path), done, totals[path]))

		deltabuffer.append((time.time(), totaldone))
		print_remaining_time(deltabuffer, totaltodo)
		print_progressbar(totaldone, totaltodo)
		if len(deltabuffer) > 50: deltabuffer.pop(0)

		print('='*80)
		if once: 
			run = False
			continue
		time.sleep(interval)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', type=int, default=10, help='update interval')
	parser.add_argument('--once', action='store_true', help='print once')
	parser.add_argument('d2dir')

	args = parser.parse_args()

	if not os.path.isdir(args.d2dir): raise IOError('d2dir does not exist')

	paths = []
	for subdir in os.listdir(args.d2dir):
		if 'vs' in subdir: paths.append('{}/{}'.format(args.d2dir, subdir))

	main(paths, interval=args.i, once=args.once)
