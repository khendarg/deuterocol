#!/usr/bin/env python
from __future__ import print_function, division
import argparse
import re
import os
import datetime
import subprocess
try: 
	import urllib.request
	urlopen = urllib.request.urlopen
except ImportError:
	import urllib
	urlopen = urllib.urlopen
import csv
from kevtools_common.types import TCID

def load(infile='fast_dbtool/structure_subunits'):
	if infile is None: pass #remote stuff

	else:
		result = []
		keys = []
		f = open(infile)
		csv_reader = csv.reader(f, dialect='excel')

		firstrow = True
		for row in csv_reader:
			if firstrow:
				keys.extend(row)
				firstrow = False
			else:
				obj = {}
				for k, v in zip(keys, row):
					obj[k] = v
				result.append(obj)
	return result

def extract_pdbc(rows):
	result = []
	for obj in rows:
		try: 
			pdbid = re.findall('[0-9][a-z][0-9a-z][0-9a-z]', obj['pdbid'].lower())[0]
		except IndexError: 
			EXCEL_STINKS = float(obj['pdbid'].replace('=', '').replace('"', ''))
			pdbid = '{:0.0e}'.format(EXCEL_STINKS).replace('+', '')
		chain = obj['protein_letter']
		
		result.append('{}_{}'.format(pdbid.upper(), chain))
	return result

def rotate_blastdb(outdir):
	if not os.path.isdir(outdir): os.mkdir(outdir)
	if not os.path.isdir('{}/current'.format(outdir)): 
		os.mkdir('{}/current'.format(outdir))
		subprocess.call(['extractFamily.pl', '-i', 'all', '-f', 'blast', '-o', '{}/current'.format(outdir)])
		return

	lastmod = datetime.date.fromtimestamp(os.path.getmtime('{}/current'.format(outdir)))
	now = datetime.date.today()
	if lastmod != now:
		os.rename('{}/current'.format(outdir), '{}/{}'.format(outdir, str(lastmod)))
		subprocess.call(['extractFamily.pl', '-i', 'all', '-f', 'blast', '-o', '{}/current'.format(outdir)])

	if not os.path.isfile('{}/current/tcdb.psq'.format(outdir)):
		subprocess.call(['extractFamily.pl', '-i', 'all', '-f', 'blast', '-o', '{}/current'.format(outdir)])

def tcblast(pdblist, blastprefix, expect=1e-5, ident=0.80):

	if not os.path.isfile('{}/current/sequences.faa'.format(blastprefix)):

		postdata = 'db=protein&retmode=text&rettype=fasta&id='
		s = ''
		for pdbc in pdblist:
			s += '{},'.format(pdbc)
		s = re.sub(',$', '', s)
		postdata += s

		f = urlopen('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi', data=postdata)

		g = open('{}/current/sequences.faa'.format(blastprefix), 'w')
		g.write(f.read())

	if not os.path.isfile('{}/current/tcblast.tsv'.format(blastprefix)):
		cmd = ['blastp', '-db', '{}/current/tcdb'.format(blastprefix),
			'-evalue', str(expect),
			'-max_target_seqs', '1',
			'-outfmt', '6', 
			'-query', '{}/current/sequences.faa'.format(blastprefix),
			'-out', '{}/current/tcblast.tsv'.format(blastprefix)
		]
		subprocess.call(cmd)

	mapping = {}
	with open('{}/current/tcblast.tsv'.format(blastprefix)) as f:
		for l in f:
			sl = l.split('\t')
			junk, pdbid, chain = sl[0].split('|')
			tcid = sl[1]
			pdbc = '{}_{}'.format(pdbid, chain)
			if pdbc not in mapping:
				mapping[pdbc] = tcid

	result = {}
	for k in mapping: 
		try: result[TCID(mapping[k])].append(k)
		except KeyError: result[TCID(mapping[k])] = [k]
	return result
	

def main(expect=1e-5, ident=0.80, outfile='/dev/stdout', blastprefix='blast'):
	rotate_blastdb(blastprefix)

	rows = load()
	pdblist = extract_pdbc(rows)

	tcmap = tcblast(pdblist, blastprefix, expect=expect, ident=ident)

	with open(outfile, 'w') as f:
		for tcid in sorted(tcmap):
			line = str(tcid)
			line += '\t'
			s = ''
			for pdbc in sorted(tcmap[tcid]):
				s += '{},'.format(pdbc)
			line += re.sub(',$', '', s)
			f.write(line + '\n')

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-e', type=float, default=1e-5, help='Maximum e-value (default:1e-5)')
	parser.add_argument('-i', type=float, default=0, help='Minimum percent identity (default:0)')

	parser.add_argument('-o', default='blast', help='Where BLAST databases and saved data are (to be) stored (default:blast)')

	parser.add_argument('outfile', nargs='?', default='/dev/stdout', help='Where to write the table (default:stdout)')

	args = parser.parse_args()

	main(expect=args.e, ident=args.i/100, outfile=args.outfile, blastprefix=args.o)
