#!/usr/bin/env python

import argparse
import os
import json
from kevtools_common.types import TCID
import numpy as np

def count_residues(fh):
	resi = set()
	for l in fh:
		if l.startswith('ATOM'):
			resi.add(l[22:26])
	return len(resi)

def main(deutdir):
	with open('{}/deuterocol1/pdblist.json'.format(deutdir)) as fh:
		pdblist = json.load(fh)

	revpdblist = {}
	for tcid in pdblist:
		for pdbc in pdblist[tcid]:
			revpdblist[pdbc] = TCID(tcid)

	rescounts = {}
	for bn in os.listdir('{}/cut_pdbs'.format(deutdir)):
		#pdbc = bn[:6]
		tcid = revpdblist[bn[:6]]
		with open('{}/cut_pdbs/{}'.format(deutdir, bn)) as fh:
			nres = count_residues(fh)
			if tcid not in rescounts: rescounts[tcid] = []
			rescounts[tcid].append(nres)

	return rescounts


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('deutdir')
	args = parser.parse_args()

	rescounts = main(args.deutdir)
	for tcid in sorted(rescounts):
		print('{tcid}: {mean:0.1f}+/-{std:0.1f}aa (N={count:d}, total={total:d})'.format(tcid=tcid, mean=np.mean(rescounts[tcid]), std=np.std(rescounts[tcid]), count=len(rescounts[tcid]), total=np.sum(rescounts[tcid])))
