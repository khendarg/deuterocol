from __future__ import print_function, division
import argparse
import os
import dstats

def main(infiles, method=None, number=1, overlap=False):
	alignments = dstats.winnow(infiles, method=method, number=number, overlap=overlap)
	for aln in alignments: print(aln)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('--method', help='Alignment sorting parameter. Implemented: \033[1mcovqual\033[0m, covrmsd, quality, coverage, rmsd, length, alphabet, euclid1, euclid2, euclid3, euclid4')
	parser.add_argument('-n', default=1, type=int, help='Number of alignments to collect for each pair of chains')
	parser.add_argument('--overlap', action='store_true', help='Allow overlapping alignments to show up')

	parser.add_argument('infile', nargs='+', help='TSV tables or Deuterocol directories to scour')

	args = parser.parse_args()

	main(args.infile, method=args.method, number=args.n, overlap=args.overlap)
