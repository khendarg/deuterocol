#!/usr/bin/env python

import argparse
import os
import json
import gzip

def main(args):
	index = {}
	for bn in os.listdir(args.deutdir):
		if '_vs_' in bn:
			with open('{}/{}/tmalignments/sp_all.tsv'.format(args.deutdir, bn)) as fh:
				for l in fh:
					index[l[:l.find('\t')]] = bn
	with gzip.open('{}/index.json.gz'.format(args.deutdir), 'wb') as fh:
		fh.write(json.dumps(index, indent=1).encode('utf-8'))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('deutdir')
	args = parser.parse_args()

	main(args)
