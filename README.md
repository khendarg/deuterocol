# deuterocol

A suite of membrane protein structure comparison tools

## Recommended usage

### deuterocol.py ###

#### Command-line arguments ####

`--config`
	(Optional) Load config file. 
	If `deuterocol.py` is run on an existing file, it will interpret said file as a config file.

`--fams1`
	First set of families.

`--fams2`
	Second set of families.
	(Not implemented): Specify "negative" to generate a negative control automatically.

`--force`
	Force overwriting existing files.

`--allow-internal`
	Allow within-TCID comparisons.

`--bundle`
	Number of TMSs in a bundle.

`--children`
	TC-ID depth to split families by.

`--tmdata`
	TMData directory. Reads from $TMDATA if set.

`--min-tms`
	Minimum TMSs allowed in a structure to be aligned.

`--skip-cut`
	Skip the cutting step on the alignment tool. Will be deprecated once cut detection is improved.

`--ignore-empty`
	Ignore segfaults issued by TMalign.
	Currently, it appears the occasional ATOMless structure (generally from chains present in the biological assembly but absent from the coordinates file) is responsible for causing these.

#### Config files ####

```cfg
#These lines set the families to be compared
set fams1 1.A.1 
set fams2 1.A.2

#This line sets the bundle size
set bundle 4

#TC-ID granularity: Subfamily (1.A.1.1, 1.A.1.2, 1.A.2.1, 1.A.2.4, etc.)
set children 4

#Let deuterocol.py know where tmdata is stored
set tmdata /db/tmdata

#Finally, set an output directory
set outdir demo_1.A.1_vs_1.A.2
```

## Stepwise scripts

These scripts collect, record, and align structures given TCDB families/subfamilies. 
`deuterocol.py` is recommended over running these scripts separately for most applications where TMS corrections, family reassignments, and alignment index adjustments are *not* needed for the sake of organization and ease of use, but each script in the pipeline is capable of standalone operation.

### Deuterocol1.py ###

This script collects available indices and coordinates files for any number of TCDB superfamilies/families/subfamilies/systems.
It can also attempt to generate a negative control of comparable size to an existing set of positive control structures.

`--fams`
	A list of TC-IDs corresponding to the superfamilies, families, subfamilies, or systems of interest

`-o OUTDIR`
	Where to store the resulting data (default: deuterocol1)

`--invert-inclusive`
	Creates a negative control from excluded subfamilies belonging to known superfamilies

`--invert-inclusive-size FACTOR`
	Multiplies the effective size of the positive control by this value to generate a negative control of adjusted size

`--min-tms`
	Minimum number of TMSs for negative control proteins

`--max-tms`
	Maximum number of TMSs for negative control proteins

`tmdatadir`
	Where TMS assignment data is stored

### Deuterocol2.py ###

This script prepares a directory structure for bulk structure comparisons

`--fams1, --fams2`
	Lists of TC-IDs corresponding to the superfamilies, families, subfamilies, or systems to compare against each other

`--d1dir`
	Path to Deuterocol1 directory

`--tmdatadir`
	Path to TMdata directory

`--outdir`
	Path to write to

`--allow-internal`
	Allow self-vs-self comparisons, e.g. 1.A.1 vs 1.A.1

### superpose.py ###

<!--CITEME-->
This wrapper for SSM Superpose runs superpose on all scheduled alignments for a given Deuterocol2 output directory.
Can be safely run multiple times on the same directory without clobbering

`d2dir`
	Path to Deuterocol2 directory

`--skip-cut`
	Skip the structure-cutting step

`--min-tms`
	If at least one structure has fewer than this many TMSs, skip the alignment

### tmalign.py ###

This wrapper for TMalign runs TMalign on all scheduled alignments for a given Deuterocol2 output directory.
Can be safely run multiple times on the same directory without clobbering

`d2dir`
	Path to Deuterocol2 directory

`--skip-cut`
	Skip the structure-cutting step

`--min-tms`
	If at least one structure has fewer than this many TMSs, skip the alignment

### winnower ###

This script selects the best superposition(s) for each pair of polypeptides after filtering by coverage, length, or quality.

`infile`
	A single superposition TSV or a Deuterocol2 output path

`outfile`
	Where to write the surviving alignments

`-d`
	Distance threshold for coverage calculations (default: 4ang)

`--min-cov`
	Minimum coverage (0...1.)

`-s`
	How many extra alignments to keep for each pair of structures (default:0)

`--tmdatadir`
	Path to TMdata directory

## TMdatabase management

These scripts retrieve various TMS predictions from various databases.
Downloading pregenerated copies of the TMData files is recommended over running the scripts individually.

### opm\_dbtool ###

This script grabs OPM TMS assignments and turns them into per-chain TMS assignments.
Due to anomalies in the TMS index format, some manual intervention on `ASSIGNMENTS.TSV` is currently necessary to make the output intelligible.

### pdbtm\_dbtool ###

This script fetches and unpacks PDBTM TMS assignments

### superfamily\_dbtool ###

This script parses the text on the TCDB [http://tcdb.org/superfamily.php](list of superfamilies) in order to allow for automated negative control generation based on superfamilies.

## Auxiliary scripts ##

### json2tsv.py ###

Use this to convert the resulting alignment JSONs to TSVs with frequently used information.

### progress.py ###

Run this on a Deuterocol2 (or deuterocol.py-generated) directory to periodically print out finished and scheduled alignments.

### quomodo.py ###

#### --local ####

Run this on a Deuterocol results directory along with an alignment name of interest to open a Pymol visualization of the alignment.

#### --client ####

Pipe the output of running `quomodo.py --server` to a locally run `quomodo.py --client` to open a Pymol visualization of the alignment.
This can typically be done using something like the following:
```
ssh user@remote "quomodo.py --server /path/to/results/directory 1PDB_A_h1-1_vs_2PDB_A_h1-1" | tee /dev/tty | quomodo.py --client
```

#### --server ####

Run this on a Deuterocol results along with an alignment name of interest to dump the data needed to open a Pymol visualization of the alignment.

### uniquomodo.py ###

Run this on a Deuterocol2 (or deuterocol.py-generated) directory along with the alignment name of interest to open a Pymol visualization of the alignment.
